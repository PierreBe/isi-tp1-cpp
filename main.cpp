#include <iostream>
using namespace std;

#include <conio.h>	// pour _kbhit() et _getch()
#include <time.h>	// pour time(int)
#include <stdlib.h>	// pour srand(int) et rand()
#include "RockUtiles.h"	// pour la gestion de l'écran

// g++ *.cpp -I .

// Partie I
// --------
void afficherTerrain( int nbLignes, int nbColonnes );
int recupererTouche();
int calculerDirectionTouche( int touche );
void positionAleatoire( int nbLignes, int nbColonnes, int &posX, int &posY );
void deplacerSerpentI( int direction, int &posX, int &posY );

// Partie II
// ---------
int saisirNiveau();
void creerSouris(	int nbLignes, int nbColonnes,
			int sourisX[], int sourisY[], int nbSouris );
void afficherSouris( int sourisX[], int sourisY[], int nbSouris);
void deplacerSerpentII(	int direction, int serpentX[], int serpentY[],
				int &tailleSerpent,
				int sourisX[], int sourisY[], int &nbSouris );
/*	ou	*/
//void deplacerSerpentII(	int direction, int serpentX[], int serpentY[],
//				int &indiceTete, int &indiceQueue,
//				int sourisX[], int sourisY[], int &nbSouris );
bool testerCollision( int x, int y, int sourisX[], int sourisY[], int &nbSouris );


// Partie III (BONUS)
// ------------------
void deplacerSouris( int sourisX[], int sourisY[], int nbSouris );
void placerMurs( int nbLignes, int nbColonnes, int nbMurs );

int saisir_murs();//TEST


//  fonction principale
//  -------------------
int main ()
{
	SetConsoleTitle("Snake");

	srand(time(NULL));
	const int HT_TERRAIN=30,LG_TERRAIN=70;

	CONSOLE_FONT_INFOEX cfi={sizeof(cfi)};
	cfi.dwFontSize.X=8;
	cfi.dwFontSize.Y=8;
	//cfi.ptMinTrackSize.x = LG_TERRAIN;//
    //cfi.ptMinTrackSize.y = HT_TERRAIN;//
	SetCurrentConsoleFontEx(handle,FALSE,&cfi);

	int nb_souris=1,x_souris[20]={0},y_souris[20]={0},x_serpent[20+1]={0},y_serpent[20+1]={0},taille_serpent,nb_murs=0;

	curseurVisible(0);
	while(nb_souris!=0){
		taille_serpent=1;
		afficherTerrain(HT_TERRAIN,LG_TERRAIN);
		nb_murs=saisir_murs();//TEST
		nb_souris=saisirNiveau();
		if(nb_souris!=0){
			placerMurs(HT_TERRAIN,LG_TERRAIN,nb_murs);//TEST
			positionAleatoire(HT_TERRAIN,LG_TERRAIN,x_serpent[0],y_serpent[0]);
			creerSouris(HT_TERRAIN,LG_TERRAIN,x_souris,y_souris,nb_souris);
			afficherSouris(x_souris,y_souris,0);// VERIFIER SI ELLE N'EST PAS SUR UN MUR OU SUR LE SERPENT ?
			deplacerSerpentII(-1,x_serpent,y_serpent,taille_serpent,x_souris,y_souris,nb_souris);
		}
		// MESSAGE : GAGNE /PERDU ?
		gotoXY(0,HT_TERRAIN+1);
		system("pause");
	}
	color(15);
	system("cls");
	curseurVisible(1);
}




// ************
// * PARTIE I *
// ************
void afficherTerrain( int nbLignes, int nbColonnes )
/*
	Tâche: afficher le contour du terrain
	Paramètres: le nombre de lignes et de colonnes du terrain rectangulaire
*/
{
	//setDimensionFenetre(0,0,nbColonnes,nbLignes);
	system("cls");
	cadre(0,0,nbColonnes,nbLignes,15);
}

int recupererTouche()
/*
	Tâche: tester si le joueur a appuyé sur une touche
	Retour: retourner le code ASCII de la touche pressée, -1 sinon
*/
{
	if(_kbhit()){
		return _getch();
	}else{
		return -1;
	}
}

int calculerDirectionTouche( int touche )
/*
	Tâche: calculer la direction correspondant à une touche ou -1
	Paramètre: le code d'une touche (w, a, s ou d)
	Retour: la direction qui correspond à la touche 
		(0: droite, 1: gauche, 2: haut, 3: bas)
*/
{
	switch(touche){
		case 27:return 4;break;	// esc
		case 65:return 1;break;	// A
		case 68:return 0;break;	// D
		case 80:return 5;break;	// P
		case 83:return 3;break;	// S
		case 87:return 2;break;	// W
		case 65+32:return 1;break;	// a
		case 68+32:return 0;break;	// d
		case 80+32:return 5;break;	// p
		case 83+32:return 3;break;	// s
		case 87+32:return 2;break;	// w
		default:return -1;break;
	}
}

void positionAleatoire( int nbLignes, int nbColonnes, int &posX, int &posY )
/*
	Tâche: calculer une position aléatoire sur le terrain
	Paramètres: les dimensions du terrain en entrée et les coordonnées de la position aléatoire en sortie
*/
{
	//srand(time(NULL));
	posX=rand()%(nbColonnes-1)+1;
	posY=rand()%(nbLignes-1)+1;
}

void deplacerSerpentI( int direction, int &posX, int &posY )
/*
	Tâche: déplacer le serpent d'une seule case dans la direction donnée. Le serpent est à l'écran avant l'appel et au retour de la fonction
	Paramètres: la direction du serpent en entrée, et la position du serpent en entrée / sortie
*/

{
	int prevdir=-1;
	while(direction!=4){
		if(_kbhit()){
			direction=calculerDirectionTouche(recupererTouche());
			if(direction!=-1&&(direction==0&&prevdir!=1||direction==1&&prevdir!=0||direction==2&&prevdir!=3||direction==3&&prevdir!=2)){
				prevdir=direction;
			}else if (direction!=4&&direction!=prevdir){
				direction=prevdir;
			}
		}
		gotoXY(posX,posY);
		cout<<(char)0;
		switch(direction){
			case 0:posX++;break;	// d
			case 1:posX--;break;	// a
			case 2:posY--;break;	// w
			case 3:posY++;break;	// s
		}
		if(getCharXY(posX,posY)==(char)186||getCharXY(posX,posY)==(char)205){
			direction=4;
		}else{
			gotoXY(posX,posY);
			color(255);
			cout<<(char)0;
			color(15);
			Sleep(100);
		}
	}
}




// *************
// * PARTIE II *
// *************
int saisirNiveau()
/*
	Tâche: lire le niveau de difficulté avec tests de validation d'entrée
	Retour: le niveau (= le nombre de souris initialement sur le terrain)
*/
{
	const short MIN=1,MAX=20;
	short niveau=0;
	gotoXY(2,2);
	cout<<"Saisir le niveau ("<<MIN<<" à "<<MAX<<", 0 pour quitter) : ";
	cin>>niveau;

	while(cin.fail()||cin.peek()!='\n'||niveau<MIN&&niveau!=0||niveau>MAX){
		cin.clear();
		cin.ignore(512,'\n');
		gotoXY(2,2);
		for(short i=2;i<70;i++){
			cout<<" ";
		}
		gotoXY(2,2);
		cout<<"ERREUR ! Saisir le niveau ("<<MIN<<" à "<<MAX<<", 0 pour quitter) : ";
		cin>>niveau;
	}
	gotoXY(2,2);
	for(short i=2;i<70;i++){
		cout<<(char)0;
	}
	return niveau;
}

void creerSouris(	int nbLignes, int nbColonnes,
		int sourisX[], int sourisY[], int nbSouris )
/*
	Tâche: générer les nbSouris aléatoirement sur le terrain
	Paramètres: les dimensions du terrain, les tableaux de coordonnées et le 
			nombre de souris
*/
{
	for(short i=0;i<nbSouris;i++){
		positionAleatoire(nbLignes,nbColonnes,sourisX[i],sourisY[i]);
	}
}

void afficherSouris( int sourisX[], int sourisY[], int nbSouris)
/*
	Tâche: afficher les souris
	Paramètres: les tableaux de coordonnées et le nombre de souris
*/
{
	gotoXY(sourisX[nbSouris],sourisY[nbSouris]);
	cout<<"*";
}

void deplacerSerpentII(	int direction, int serpentX[], int serpentY[], int &tailleSerpent, int sourisX[], int sourisY[], int &nbSouris )
/*
	Tâche: déplacer le serpent d'une seule case dans la direction donnée. Le 			serpent est à l'écran avant l'appel et au retour de la fonction
	Paramètres: en entrée : 	la direction du serpent,
			en entrée/sortie :	les tableaux de coordonnées du serpent,
				la taille du serpent, 
				les tableaux de coordonnées des souris et
				le nombre de souris
*/
{
	int prevdir=-1;
	const unsigned short LENTEUR_SOURIS=4;
	unsigned int souris_bouge=0;
	serpentX[tailleSerpent]=serpentX[0];
	serpentY[tailleSerpent]=serpentY[0];
	while(direction!=4){
		if(_kbhit()){
			direction=calculerDirectionTouche(recupererTouche());
			if(direction==5){
				// goto xy cout pause
				while(!_kbhit()){/*wait*/}
				// goto xy cout "     "
				direction=prevdir;
			}else if(direction!=-1&&(direction==0&&prevdir!=1||direction==1&&prevdir!=0||direction==2&&prevdir!=3||direction==3&&prevdir!=2)){
				prevdir=direction;
			}else if(direction!=4&&direction!=prevdir){
				direction=prevdir;
			}
		}
		gotoXY(serpentX[tailleSerpent],serpentY[tailleSerpent]);	// avancer queue serpent
		cout<<(char)0;	// avancer queue serpent
		switch(direction){
			case 0:serpentX[0]++;break;	// d
			case 1:serpentX[0]--;break;	// a
			case 2:serpentY[0]--;break;	// w
			case 3:serpentY[0]++;break;	// s
		}
	
		if(getCharXY(serpentX[0],serpentY[0])==(char)186||getCharXY(serpentX[0],serpentY[0])==(char)205||getCharXY(serpentX[0],serpentY[0])=='S'){	// perdre
			direction=4;
		}else{
			if(souris_bouge%LENTEUR_SOURIS==0){
				deplacerSouris(sourisX,sourisY,tailleSerpent);
			}
			if(testerCollision(serpentX[0],serpentY[0],sourisX,sourisY,tailleSerpent)){
				if(tailleSerpent>nbSouris-1){	// gagner
					direction=4;
				}else{
					tailleSerpent++;	// grandir
					afficherSouris(sourisX,sourisY,tailleSerpent-1);	// nouvelle souris
				}
			}
			for(short i=tailleSerpent;i>0;i--){
				serpentX[i]=serpentX[i-1];
				serpentY[i]=serpentY[i-1];
			}
			gotoXY(serpentX[0],serpentY[0]);
			color(255);
			cout<<'S';
			color(15);
			souris_bouge++;
			Sleep(100);
		}
	}
}

/*	ou	*/

//				int &indiceTete, int &indiceQueue,
//				int sourisX[], int sourisY[], int &nbSouris )
/*
	Tâche: déplacer le serpent dans la direction. Le 	serpent est à l'écran avant 		l'appel et au retour de la fonction
	Paramètres: en entrée :	la direction du serpent,
			en entrée/sortie :	les tableaux de coordonnées du serpent, les 				indices de tête et de queue du serpent,
				les tableaux de coordonnées des souris et
				le nombre de souris
*/
//{
	// à compléter
//}

bool testerCollision(	int posX, int posY,
				int sourisX[], int sourisY[], int &nbSouris)
/*
	Tâche: tester si (posX, posY) est la position d'une souris. Si oui, retirer 
		la souris de la population de souris
	Paramètres: la position de la tête du serpent, les tableaux de coordonnées
			des souris et le nombre de souris
	Retour: true si collision, false sinon
*/
{
	if(posX==sourisX[nbSouris-1]&&posY==sourisY[nbSouris-1]){
		return 1;
	}
	return 0;
}






// **********************
// * PARTIE III : BONUS *
// **********************

void deplacerSouris(int sourisX[], int sourisY[], int nbSouris)
/*
	Tâche: déplacer les souris (aléatoirement ou intelligemment)
	Paramètres: les tableaux de coordonnées et le nombre de souris
*/
{
	short direction=0,dx=0,dy=0;
	gotoXY(sourisX[nbSouris-1],sourisY[nbSouris-1]);
	cout<<(char)0;
	direction=rand()%4;
	switch(direction){
		case 0:dx++;break;
		case 1:dx--;break;
		case 2:dy--;break;
		case 3:dy++;break;
	}
	if(getCharXY(sourisX[nbSouris-1]+dx,sourisY[nbSouris-1]+dy)!=(char)186&&getCharXY(sourisX[nbSouris-1]+dx,sourisY[nbSouris-1]+dy)!=(char)205&&getCharXY(sourisX[nbSouris-1]+dx,sourisY[nbSouris-1]+dy)!='S'){
		sourisX[nbSouris-1]+=dx;
		sourisY[nbSouris-1]+=dy;
	}
	afficherSouris(sourisX,sourisY,nbSouris-1);
}

void placerMurs(int nbLignes, int nbColonnes, int nbMurs)
/*
	Tâche: placer des murs aléatoirement sur le terrain de jeu
	Paramètres: les dimensions du terrain et le nombre de murs à placer
*/
{
	const short DEGAGEMENT=5;
	for(short i=0;i<nbMurs;i++){
		short pos[2][2]={0},increment=1;
		for(short j=0;j<2;j++){
			pos[j][0]=rand()%(nbColonnes-1-DEGAGEMENT*2)+1+DEGAGEMENT;
			pos[j][1]=rand()%(nbLignes-1-DEGAGEMENT*2)+1+DEGAGEMENT;
		}

		//regression lineaire
		if((pos[1][0]-pos[0][0])*(pos[1][0]-pos[0][0])>(pos[1][1]-pos[0][1])*(pos[1][1]-pos[0][1])){	// si + d'ecart en x qu'en y
			if(pos[1][0]-pos[0][0]<0){	// si x1 > x2
				increment=-1;
			}
			for(short j=pos[0][0];j!=pos[1][0]+increment;j+=increment){
				gotoXY(j,pos[0][1]+(j-pos[0][0])*(pos[1][1]-pos[0][1])/(pos[1][0]-pos[0][0]));
				cout<<(char)205; //ligne horizontale cout<<(char)186;  //ligne verticale
			}
		}else{
			if(pos[1][1]-pos[0][1]<0){	// si y1 > y2
				increment=-1;
			}
			for(short j=pos[0][1];j!=pos[1][1]+increment;j+=increment){
				gotoXY(pos[0][0]+(j-pos[0][1])*(pos[1][0]-pos[0][0])/(pos[1][1]-pos[0][1]),j);
				cout<<(char)186;  //ligne verticale cout<<(char)205; //ligne horizontale
			}
		}
	}
}

int saisir_murs(){
	const short MIN=0,MAX=2;
	short nb_murs=0;
	gotoXY(2,2);
	cout<<"Saisir le nombre de murs ("<<MIN<<" à "<<MAX<<") : ";
	cin>>nb_murs;

	while(cin.fail()||cin.peek()!='\n'||nb_murs<MIN||nb_murs>MAX){
		cin.clear();
		cin.ignore(512,'\n');
		gotoXY(2,2);
		for(short i=2;i<70;i++){
			cout<<" ";
		}
		gotoXY(2,2);
		cout<<"ERREUR ! Saisir le nombre de murs ("<<MIN<<" à "<<MAX<<") : ";
		cin>>nb_murs;
	}
	gotoXY(2,2);
	for(short i=2;i<70;i++){
		cout<<(char)0;
	}
	return nb_murs;
}